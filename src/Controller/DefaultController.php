<?php

namespace App\Controller;


use App\Entity\User;
use App\FormTypes\LoginType;
use App\FormTypes\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * Matches / exactly
     * @Route("/", name="index")
     * @return Response
     */
    public function index()
    {
        if ($this->getUser() === null)
            return $this->redirectToRoute('login');
        dump($this->getUser());
        return $this->render('index.html.twig');
    }

    /**
     * Matches /admin exactly
     * @Route("/admin", name="admin")
     * @return Response
     */
    public function admin()
    {
        return new Response('<html><body>Admin page!</body></html>');
    }
}
