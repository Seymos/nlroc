<?php

namespace App\Controller;


use App\Entity\Course;
use App\FormTypes\CourseType;
use Cocur\Slugify\Slugify;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route as Route;


class AccountController extends Controller
{
    /**
     * Matches /account exactly
     * @Route("/account", name="account")
     */
    public function account()
    {
        if ($this->getUser() === null)
            $this->redirectToRoute('login');
        $courses = $this->getDoctrine()->getRepository(Course::class)->findBy(
            array(
                'user' => $this->getUser()
            )
        );
        return $this->render(
            'account.html.twig',
            array(
                'user' => $this->getUser(),
                'courses' => $courses
            )
        );
    }

    /**
     * Matches /upload exactly
     * @Route("/upload", name="upload")
     * @param Request $request
     * @return Response
     */
    public function upload(Request $request)
    {
        $course = new Course();
        $form = $this->createForm(CourseType::class, $course);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $course->setSlug("parcours-1");
            $course->setUser($this->getUser());
            // $file stores the uploaded PDF file
            /** @var UploadedFile $file */
            $file = $course->getFile();
            $fileName = $course->getName().'.'.$file->guessExtension();
            /*$file->move(
                $this->getParameter('file_directory'),
                $fileName
            );*/
            $course->setFile($fileName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($course);
            $em->flush();;
            $this->addFlash('success',"Upload réussi !");
            return $this->redirectToRoute('account');
        }
        return $this->render(
            'upload_course.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        return md5(uniqid());
    }
}