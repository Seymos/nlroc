<?php

namespace App\Controller;


use App\Entity\User;
use App\FormTypes\LoginType;
use App\FormTypes\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{
    /**
     * Matches /login exactly
     * @Route("/login", name="login")
     * @param Request $request
     * @param AuthenticationUtils $authUtils
     * @return Response
     */
    public function login(Request $request, AuthenticationUtils $authUtils)
    {
        $error = $authUtils->getLastAuthenticationError();
        $lastUsername = $authUtils->getLastUsername();
        return $this->render(
            'login.html.twig',
            array(
                'error' => $error,
                'last_username' => $lastUsername
            )
        );
    }

    /**
     * Matches /logout exactly
     * @Route("/logout", name="logout")
     */
    public function logout()
    {}

    /**
     * Matches /register exactly
     * @Route("/register", name="register")
     * @param Request $request
     * @return Response
     */
    public function register(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $repository = $this->getDoctrine()->getRepository(User::class);
            // vérification -> service
            $one = $repository->findOneBy(array('email' => $form->getData()->getEmail()));
            if ($one !== null) {
                $this->addFlash('danger',"Un utilisateur existe pour cet email");
                return $this->redirectToRoute('register');
            }
            // service pour ces actions
            $user->setActive(false);
            $user->setRoles(array('ROLE_USER'));
            $user->setToken();
            // service envoi email confirmation avec token
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash('success',"Votre compte a été créé ! Veuillez vérifier votre boîte e-mail afin d'activer votre compte");
            return $this->redirectToRoute('registration_confirmed');
        }
        return $this->render(
            'register.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }

    /**
     * Matches /registration-confirmed exactly
     * @Route("/registration-confirmed", name="registration_confirmed")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function confirmRegistration()
    {
        return $this->render('confirm_registration.html.twig');
    }

    /**
     * Matches /activate/*
     * @Route("/activate/{token}", name="activate_account")
     * @param $token
     * @return Response
     */
    public function activateAccount($token)
    {
        $repository = $this->getDoctrine()->getRepository(User::class);
        $user = $repository->findOneBy(array('active' => 0, 'token' => $token));
        if (!$user) {
            $this->addFlash(
                'error', /** @lang text */
                "Désolé mais ce lien n'est pas valable");
            return $this->redirectToRoute('register');
        }

        if ($user->getToken() !== $token) {
            $this->addFlash('danger',"Ce lien n'est plus valable, merci de renvoyer l'e-mail de confirmation via votre espace personnel");
            return $this->redirectToRoute('index');
        }
        $user->resetToken();
        $user->setActive(true);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        $this->addFlash('success',"Votre compte a été activé avec succès !");
        return $this->redirectToRoute('index');
    }
}
