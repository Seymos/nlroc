<?php

namespace App\FormTypes;


use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'username',
                TextType::class,
                array(
                    'label' => "Nom d'utilisateur",
                    'attr' => array(
                        'class' => "form-control"
                    )
                )
            )
            ->add(
                'email',
                EmailType::class,
                array(
                    'label' => "Email",
                    'attr' => array(
                        'class' => "form-control"
                    )
                )
            )
            ->add(
                'password',
                RepeatedType::class,
                array(
                    'label' => "",
                    'type' => PasswordType::class,
                    'invalid_message' => "Les mots de passe doivent correspondre",
                    'required' => true,
                    'first_options' => array(
                        'label' => "Mot de passe",
                        'attr' => array(
                            'class' => "form-control"
                        )
                    ),
                    'second_options' => array(
                        'label' => "Répéter le mot de passe",
                        'attr' => array(
                            'class' => "form-control"
                        )
                    )
                )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => User::class
            )
        );
    }
}
